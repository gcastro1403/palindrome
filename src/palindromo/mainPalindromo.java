package palindromo;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class mainPalindromo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		isPalindrome("civic");
	}
	
	public static boolean isPalindrome( String wordPalindrome ) {
		String [] wordInParts= wordPalindrome.split("");
		Queue<String> queueWord = new LinkedList<>(Arrays.asList(wordInParts));
		ArrayDeque<String> dequeWord = new ArrayDeque<>(Arrays.asList(wordInParts));
		String pollLetter = queueWord.poll();
		String pollLetterD = dequeWord.pollLast();
		boolean isTheSame = pollLetter.equals(pollLetterD)? true : false;
		//System.out.print(isTheSame);
		return isTheSame;
		
	}

}
