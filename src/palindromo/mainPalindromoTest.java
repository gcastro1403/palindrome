package palindromo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class mainPalindromoTest {
	
	 mainPalindromo palindromo = new mainPalindromo();

	@Test
	void isPalindrometestFalse() {
		boolean isPalindrome = mainPalindromo.isPalindrome("Gaby");
		assertFalse(isPalindrome);
		
	}
	@Test
	void isPalindrometestTrue() {
		boolean palindrome = mainPalindromo.isPalindrome("civic");
		assertTrue(palindrome);
	}
		

}
